## About
This application is the backend part of a project: https://gitlab.com/jagrcarl/pokemonspotter
It is hosted on Heroku and endpoints are used to add to a database: https://pokemon-spotter-db.herokuapp.com/

The application is a Spring Boot application that uses Hibernate to create a PostgreSQL database. 
The database stores users and pokemons. 

By: Jessica, Kristoffer & Calle


Dev TODO:
- Add endpoint to get user's overall ranking (not just points?)

## !!! IMPORTANT !!! ON START-UP
If the db is empty, do a GET request to /startup
(populates tables & is NOT supposed to be used by user)

## Endpoints
- Add a user to the db (POST) - /api/user 

Format:
{
    "accountName": "username",
    "email": "user.123@gmail.com",
    "password": "123456"
}

- Get the user with the given id from the db (GET) - /api/user/{id}

- Get the user with the given username from the db (GET) - /api/user/name/{name}

- Get the user's points (GET) - /api/user/points

- Get all users from the db (GET) - /api/users

- Get a list of the given user's spotted pokemons (GET) - /api/user/{userId}/pokemons 

- Get a list of the given user's unspotted pokemons (GET) - /api/user/{userId}/pokemons/unspotted 

- Get a list of top 10 users & their points (GET) - /api/user/ranks

- Add a pokemon to the db (POST) - /api/user/{userId}/pokemon 

Format:
{
    "name": "mew",
    "gender": "female",
    "type": "someType",
    "isShiny": true,
    "longitude" : "14",
    "latitude" : "145",
    "date": "2020-10-12 12:00:00"
}


- Get the pokemon with the given id from the db (GET) - /api/pokemon/{id} 

- Get a list of all pokemons from the db (GET) - /api/pokemons

- Get a list of the 10 rarest pokemons (GET) - /api/pokemon/rares 


## Examples 
GET: https://pokemon-spotter-db.herokuapp.com/api/users

GET: https://pokemon-spotter-db.herokuapp.com/api/pokemon/rares

POST: https://pokemon-spotter-db.herokuapp.com/api/user/2/pokemon

 {
  "name": "mew",
  "gender": "female",
  "type": "grass poison",
  "isShiny": false,
  "longitude" : "14",
  "latitude" : "145",
  "date": "2020-10-12 12:20:00"
 }