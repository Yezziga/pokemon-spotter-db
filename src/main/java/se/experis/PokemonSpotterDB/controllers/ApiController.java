package se.experis.PokemonSpotterDB.controllers;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import se.experis.PokemonSpotterDB.models.*;
import se.experis.PokemonSpotterDB.repositories.*;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.*;

/**
 * Class that handles the connection to database.
 * Endpoints are used by the frontend to add users & pokemons to the database
 */
@RestController("/api")
public class ApiController {
    @Autowired
    private PokemonRepository pokemonRepo;
    @Autowired
    private WebUserRepository userRepo;
    @Autowired
    private RarityRepository rarityRepo;
    private CommonResponse cr;
    private HttpStatus status;

    /**
     * Initializes the database with data.
     * Should only be called ONCE, on startup
     */
    @GetMapping("/startup")
    public ResponseEntity<CommonResponse> initializeDb() {
        cr = new CommonResponse();
        populateRarityTable();
        populateUserTable();
        populatePokemonTable();
        cr.message = "Startup finished. Database initialized.";
        System.out.println(cr.message);

        return new ResponseEntity<>(cr, status);
    }

    /**
     * Populates the rarity table. Retrieves names for the first
     * 151 pokemons from pokeapi
     * Hardcoded data
     */
    private void populateRarityTable() {
        try {
            URL url = new URL("https://pokeapi.co/api/v2/pokemon/?limit=151&offset=0");
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");

            if (con.getResponseCode() == HttpURLConnection.HTTP_OK) {

                InputStreamReader isr = new InputStreamReader(con.getInputStream());
                BufferedReader br = new BufferedReader(isr);
                String inputLine;
                StringBuffer content = new StringBuffer();

                while ((inputLine = br.readLine()) != null) {
                    content.append(inputLine);
                }
                br.close();

                JSONObject json = new JSONObject(content.toString());

                JSONArray data = json.getJSONArray("results");
                for (int i = 0; i < data.length(); i++) {
//                  System.out.println(data.getJSONObject(i).getString("name"));
                    Rarity rarity = new Rarity();
                    rarity.pokemonName = data.getJSONObject(i).getString("name");

                    if(rarity.pokemonName.equals("bulbasaur") || rarity.pokemonName.equals("ivysaur")
                            || rarity.pokemonName.equals("venusaur")|| rarity.pokemonName.equals("charmander")
                            || rarity.pokemonName.equals("charmeleon")|| rarity.pokemonName.equals("charizard")
                            || rarity.pokemonName.equals("squirtle")|| rarity.pokemonName.equals("wartortle")
                            || rarity.pokemonName.equals("blastoise") || rarity.pokemonName.equals("caterpie")) {
                        rarity.timesSpotted = 4;
                    } else {
                        rarity.timesSpotted = 0;
                    }


                    rarityRepo.save(rarity);
                }
//                System.out.println(data);
                cr.message = "Successfully added 151 pokemons to the db";
                status = HttpStatus.OK;
            } else {
                System.out.println("Error");
                System.out.println("Server responded with: " + con.getResponseCode());
                cr.data = null;
                cr.message = "Server responded with: " + con.getResponseCode();
                status = HttpStatus.BAD_REQUEST;
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Populates the user table.
     * Hardcoded data
     */
    private void populateUserTable() {

        WebUser user = new WebUser();
        user.accountName = "Jessica";
        user.email = "jessiq@hotmail.com";
        user.password = "123455";
        userRepo.save(user);
        user = new WebUser();
        user.accountName = "Stoffe";
        user.email = "stoffe@gmail.com";
        user.password = "hejhej";
        userRepo.save(user);
        user = new WebUser();
        user.accountName = "Calle";
        user.email = "calle@live.se";
        user.password = "helloworld";
        userRepo.save(user);
        user = new WebUser();
        user.accountName = "Unicorn";
        user.email = "unithecorn@gmail.com";
        user.password = "supasafe";
        userRepo.save(user);

    }

    /**
     * Populates the pokemon table.
     * Hardcoded data
     */
    private void populatePokemonTable() {
        List<Rarity> rarities = rarityRepo.findAll();
        Pokemon pokemon;

        ArrayList<String> types = new ArrayList<>();
        types.add("grass poison");
        types.add("grass poison");
        types.add("grass poison");
        types.add("bug");
        types.add("electric flying");
        types.add("psychic");
        types.add("bug");
        types.add("normal");
        types.add("psychic fairy");
        types.add("grass poison");
        types.add("fire");
        types.add("water");
        types.add("bug flying");
        types.add("normal flying");
        types.add("normal flying");

        ArrayList<String> names = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            names.add(rarities.get(i).pokemonName);
        }

        List<WebUser> list= userRepo.findAll();
        for (WebUser user : list) {
            for (int i=0; i<10; i++) {
                pokemon = new Pokemon();
                pokemon.name = names.get(i);
                pokemon.type = types.get(i);
                pokemon.gender = "female";
                pokemon.isShiny = false;
                pokemon.longitude = "13";
                pokemon.latitude = "46";
                pokemon.date = Timestamp.valueOf(LocalDateTime.now());
                user.points+=1;

                pokemon.user = user;
                pokemonRepo.save(pokemon);
                user.pokemons.add(pokemon);
                userRepo.save(user);
            }
        }
    }

//     ##############################
//     ###### POKEMON APIS ##########
//     ##############################

    /**
     *
     * Adds a pokemon to the database.
     *
     * @param userId  id of the user adding a pokemon
     * @param pokemon the pokemon to add to db
     */
    @PostMapping("/api/user/{userId}/pokemon")
    public ResponseEntity<CommonResponse> addPokemon(@PathVariable("userId") Integer userId, @RequestBody Pokemon pokemon) {
        cr = new CommonResponse();
        HttpStatus status = HttpStatus.OK;
        if(userRepo.existsById(userId)) {
            if(!rarityRepo.existsByPokemonName(pokemon.name)) {
                cr.data = null;
                cr.message = "Pokemon with name " + pokemon.name + " cannot be added";
                status = HttpStatus.FORBIDDEN;
                return new ResponseEntity<>(cr, status);
            }
            WebUser user = userRepo.findById(userId).get();
            pokemon.user = user;
            pokemon = pokemonRepo.save(pokemon);
            user.pokemons.add(pokemon);

            if(pokemon.isShiny) {
                user.points+=5;
            } else {
                user.points+=1;
            }

            userRepo.save(user);

            Rarity rarity = rarityRepo.findByPokemonName(pokemon.name.toLowerCase());

            if (rarity == null) {
                status = HttpStatus.NOT_FOUND;
            } else {
                rarity.timesSpotted += 1;
                rarityRepo.save(rarity);
            }
            cr.data = pokemon;
            cr.message = "Pokemon added";
        } else {
            cr.data = null;
            cr.message = "User with id " + userId + " does not exist";
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(cr, status);
    }

    /**
     *
     * Adds a pokemon to the database.
     *
     * @param userName  id of the user adding a pokemon
     * @param pokemon the pokemon to add to db
     */
    @PostMapping("/api/user/name/{userName}/pokemon")
    public ResponseEntity<CommonResponse> addPokemonByUserId(@PathVariable("userName") String userName, @RequestBody Pokemon pokemon) {
        cr = new CommonResponse();
        HttpStatus status = HttpStatus.OK;
        if(userRepo.existsByAccountName(userName)) {
//            if(!rarityRepo.existsByPokemonName(pokemon.name)) {
//                cr.data = null;
//                cr.message = "Pokemon with name " + pokemon.name + " cannot be added";
//                status = HttpStatus.FORBIDDEN;
//                return new ResponseEntity<>(cr, status);
//            }
            WebUser user = userRepo.findByAccountName(userName);
            pokemon.user = user;
            pokemon = pokemonRepo.save(pokemon);
            user.pokemons.add(pokemon);

            if(pokemon.isShiny) {
                user.points+=5;
            } else {
                user.points+=1;
            }

            userRepo.save(user);

            Rarity rarity = rarityRepo.findByPokemonName(pokemon.name.toLowerCase());

            if (rarity == null) {
                status = HttpStatus.NOT_FOUND;
            } else {
                rarity.timesSpotted += 1;
                rarityRepo.save(rarity);
            }
            cr.data = pokemon;
            cr.message = "Pokemon added";
        } else {
            cr.data = null;
            cr.message = "User with username " + userName + " does not exist";
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(cr, status);
    }

    /**
     * Gets a specific pokemon from the database
     * @param id the pokemon's id in the db
     */
    @GetMapping("/api/pokemon/{id}")
    public ResponseEntity<CommonResponse> getPokemonById(@PathVariable("id") Integer id) {
        cr = new CommonResponse();
        HttpStatus response;
        if(pokemonRepo.existsById(id)) {
            cr.data = pokemonRepo.findById(id);
            cr.message = "Pokemon with id " + id;
            response = HttpStatus.OK;
        } else {
            cr.data = null;
            cr.message = "Pokemon with id " + id + " does not exist";
            response = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(cr, response);
    }

    /**
     * Returns all of the pokemons from the database
     */
    @GetMapping("/api/pokemons")
    public ResponseEntity<CommonResponse> getAllPokemons() {
        List<Pokemon> pokemonList = pokemonRepo.findAll();
        cr = new CommonResponse();
        cr.data = pokemonList;
        cr.message = "Returning list of all pokemons";
        return new ResponseEntity<>(cr, HttpStatus.OK);
    }

    /**
     * Returns a list of 10 rarest pokemons.
     */
    @GetMapping("api/pokemon/rares")
    public ResponseEntity<CommonResponse> getRarePokemons() {
        List<Rarity> rarityList = rarityRepo.findAll();

        List<Rarity> newList = new ArrayList<>();

        Collections.sort(rarityList);
        for (int i = 0; i < 10; i++) {
//            System.out.println(i + ": " + r.pokemonName + " " + r.timesSpotted);
            newList.add(rarityList.get(i));
        }

        cr = new CommonResponse();
        cr.data = newList;
        cr.message = "Returning list of rares";
        return new ResponseEntity<>(cr, HttpStatus.OK);
    }

    // ###############################
    // ########## USER APIS ##########
    // ###############################

    /**
     * Adds a user to the database.
     *
     * @param user the user to add to the db
     */
    @PostMapping("/api/user")
    public ResponseEntity<CommonResponse> addUser(@RequestBody WebUser user) {
        cr = new CommonResponse();
        HttpStatus response = HttpStatus.OK;
        cr.data = user;
        cr.message = "User added";
        userRepo.save(user);
        return new ResponseEntity<>(cr, response);
    }

    /**
     * Returns the user with the given id
     *
     * @param id user's id in the db
     */
    @GetMapping("/api/user/{id}")
    public ResponseEntity<CommonResponse> getUserById(@PathVariable("id") Integer id) {
        cr = new CommonResponse();
        HttpStatus response;
        if (userRepo.existsById(id)) {
            cr.data = userRepo.findById(id);
            cr.message = "User with id " + id;
            response = HttpStatus.OK;
        } else {
            cr.data = null;
            cr.message = "User with id " + id + " does not exist";
            response = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(cr, response);
    }

    /**
     * Returns the user with the given id
     *
     * @param name user's id in the db
     */
    @GetMapping("/api/user/name/{name}")
    public ResponseEntity<CommonResponse> getUserByName(@PathVariable("name") String name) {
        cr = new CommonResponse();
        HttpStatus response;
        WebUser user = userRepo.findByAccountName(name);
        if (user!=null) {
            cr.data = user;
            cr.message = "User with name " + name;
            response = HttpStatus.OK;
        } else {
            cr.data = null;
            cr.message = "User with name " + name + " not found";
            response = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(cr, response);
    }

    /**
     * Returns all of the users from the database
     */
    @GetMapping("/api/users")
    public ResponseEntity<CommonResponse> getUsers() {
        List<WebUser> userList = userRepo.findAll();
        cr = new CommonResponse();
        cr.data = userList;
        cr.message = "Returning all users";
        return new ResponseEntity<>(cr, HttpStatus.OK);
    }

    /**
     * Returns a list of the given user's spotted pokemons.
     *
     * @param userId id of the user to retrieve pokemons for
     */
    @GetMapping("/api/user/{userId}/pokemons")
    public ResponseEntity<CommonResponse> getUserSpottedPokemons(@PathVariable("userId") Integer userId) {
        if(userRepo.existsById(userId)) {
            List<Pokemon> pokemons = userRepo.findById(userId).get().pokemons;
            cr = new CommonResponse();
            cr.data = pokemons;
            cr.message = "Returning user " + userId + "'s pokemons";
            status = HttpStatus.OK;
        } else {
            cr.data = null;
            cr.message = "User with id " + userId + " does not exist";
            status = HttpStatus.NOT_FOUND;
        }

        return new ResponseEntity<>(cr, status);
    }

    /**
     * Returns a list of pokemons the given user have not spotted.
     *
     * @param userId id of the user to retrieve pokemons for
     */
    @GetMapping("/api/user/{userId}/pokemons/unspotted")
    public ResponseEntity<CommonResponse> getUserNotSpottedPokemons(@PathVariable("userId") Integer userId) {

        if(userRepo.existsById(userId)) {
            List<Pokemon> pokemons = userRepo.findById(userId).get().pokemons; //
            List<Rarity> rarityList = rarityRepo.findAll();
            ArrayList<Rarity> newList = new ArrayList<>();

            // Copy all 151 pokemons
            for (Rarity r : rarityList) {
                newList.add(r);
            }

            for (Pokemon p : pokemons) {
                Rarity toRemove = rarityRepo.findByPokemonName(p.name);
                if (toRemove != null) {
                    newList.remove(toRemove);
//                System.out.println(toRemove.pokemonName + " REMOVED");
                }
            }
            cr = new CommonResponse();
            cr.data = newList;
            cr.message = "Returning user " + userId + "'s unspotted pokemons";
        } else {
            cr.data = null;
            cr.message = "User with id " + userId + " does not exist";
            status = HttpStatus.NOT_FOUND;
        }

        return new ResponseEntity<>(cr, HttpStatus.OK);
    }

    @GetMapping("/api/user/{userId}/points")
    public ResponseEntity<CommonResponse> getUserPoints(@PathVariable("userId") Integer userId) {
        cr = new CommonResponse();
        if(userRepo.existsById(userId)) {
            cr.data = userRepo.findById(userId).get().points;
            cr.message = "User " + userId + "'s points";
            status = HttpStatus.OK;
        } else {
            cr.data = null;
            cr.message = "User with id " + userId + " does not exist";
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(cr, status);
    }

    @GetMapping("/api/user/ranks")
    public ResponseEntity<CommonResponse> getRankList() {
        List<WebUser> users = userRepo.findAll();

        LinkedHashMap<String, Integer> rankList = new LinkedHashMap<>();
        Collections.sort(users, Collections.reverseOrder());

        WebUser user;
        for (int i = 0; i < 10; i++) {
            if(i>=users.size()) {
                break;
            }
            user = users.get(i);
            rankList.put(user.accountName, user.points);
//            System.out.println(user.accountName + " " + user.points);
        }
        cr.data = rankList;
        cr.message = "Returning list of top 10 ranked users";
        return new ResponseEntity<>(cr, HttpStatus.OK);
    }

}
