package se.experis.PokemonSpotterDB.models;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;

@Entity
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class Rarity implements Comparable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer id;
    @Column(unique = true)
    public String pokemonName;
    @Column
    public Integer timesSpotted;

    @Override
    public int compareTo(Object o) {
        Rarity compareWith = (Rarity) o;
        int compareCount = compareWith.timesSpotted;
        return this.timesSpotted -compareCount;
    }
}
