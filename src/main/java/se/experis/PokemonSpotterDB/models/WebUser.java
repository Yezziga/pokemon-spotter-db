package se.experis.PokemonSpotterDB.models;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Entity
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class WebUser implements Comparable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer id;
    @Column(nullable = false, unique = true)
    public String accountName;
    @Column(nullable = false)
    public String email;
    @Column(nullable = false)
    public String password;
    @Column
    public Integer points = 0;
    @OneToMany(mappedBy = "user")
    public List<Pokemon> pokemons = new ArrayList<>();


    @JsonGetter("pokemons")
    public List<String> pokemons() {
        return pokemons.stream()
                .map(pokemon -> {
                    return "/api/pokemon/" + pokemon.id;
                }).collect(Collectors.toList());
    }

    @Override
    public int compareTo(Object o) {
        WebUser compareWith = (WebUser) o;
        int compareCount = compareWith.points;
        return this.points -compareCount;
    }
}
