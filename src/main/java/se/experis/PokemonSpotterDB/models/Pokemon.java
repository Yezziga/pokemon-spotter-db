package se.experis.PokemonSpotterDB.models;

import com.fasterxml.jackson.annotation.*;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class Pokemon {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer id;
    @Column(nullable = false)
    public String name;
    @Column(nullable = false)
    public String gender;
    @Column(nullable = false)
    public String type;
    @Column(nullable = false)
    public boolean isShiny;
    @Column(nullable = false)
    public String longitude;
    @Column(nullable = false)
    public String latitude;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Column(nullable = false)
    public Timestamp date;

    @ManyToOne(fetch = FetchType.EAGER)
    public WebUser user;

    @JsonGetter("user")
    public String getUser(){
        return "/api/user/" + user.id;
    }

}
