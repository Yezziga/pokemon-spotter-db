package se.experis.PokemonSpotterDB;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PokemonSpotterDbApplication {

	public static void main(String[] args) {
		SpringApplication.run(PokemonSpotterDbApplication.class, args);
	}

}
