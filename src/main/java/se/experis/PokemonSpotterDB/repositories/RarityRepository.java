package se.experis.PokemonSpotterDB.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import se.experis.PokemonSpotterDB.models.Rarity;

public interface RarityRepository extends JpaRepository<Rarity, Integer> {
    Rarity findByPokemonName(String pokemonName);
    boolean existsByPokemonName(String pokemonName);
}
