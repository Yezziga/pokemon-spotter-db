package se.experis.PokemonSpotterDB.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import se.experis.PokemonSpotterDB.models.WebUser;

public interface WebUserRepository extends JpaRepository<WebUser, Integer> {
    WebUser findByAccountName(String accountName);
    boolean existsByAccountName(String accountName);

}
