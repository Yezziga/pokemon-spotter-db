package se.experis.PokemonSpotterDB.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import se.experis.PokemonSpotterDB.models.Pokemon;

public interface PokemonRepository extends JpaRepository<Pokemon, Integer> {
    Pokemon findByName(String name);

}
